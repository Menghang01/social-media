import {
  BrowserRouter as Router,
  Switch,

} from "react-router-dom";
import PublicRoute from "./routes/PublicRoute"
import UserSignUp from "./pages/signup.js"
import Notification from "./pages/notification.js"
import Home from "./pages/home.js"
import PrivateRoute from './routes/PrivateRoute.js';
import { isAuthenticated } from "./util/auth"
import Template from "./components/layout/template"
import Message from "./pages/messgae"
import UserLogin from "./pages/login"
import ProfileScreen from "./pages/profile_screen";
import { useDispatch } from 'react-redux'
import { API_URL } from "./constant";
import Cookies from "js-cookie";
import { setProfile } from "./store/profile";
import axios from "axios";
import { useEffect } from 'react';

function App() {
  const dispatch = useDispatch();
  const callProfile = async () => {
    if (Cookies.get("userId")) {
      try {
        const response = await axios.get(API_URL + "profile/" + Cookies.get("userId"), { withCredentials: true })
        dispatch(setProfile(response.data[0]));
      }
      catch (e) {
        throw (e);
      }
    }

  }
  useEffect(() => {
    callProfile()
  }, []);
  return (
    <Router>
      <Switch>
        <PrivateRoute exact path="/" authenticated={isAuthenticated} component={Home}></PrivateRoute>
        <PrivateRoute exact path="/message" authenticated={isAuthenticated} component={Message}></PrivateRoute>
        <PrivateRoute exact path="/notification" authenticated={isAuthenticated} component={Notification}></PrivateRoute>
        <PublicRoute exact path="/login" component={UserLogin}></PublicRoute>
        <PublicRoute exact path="/signup" component={UserSignUp}></PublicRoute>
        <PublicRoute exact path="/test" component={Template}></PublicRoute>
        <PublicRoute exact path="/profile/:id" component={ProfileScreen}></PublicRoute>
      </Switch>
    </Router>

  );
}

export default App;
