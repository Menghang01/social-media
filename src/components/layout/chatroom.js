import MoreHoriz from '@material-ui/icons/MoreHoriz'
import Send from '@material-ui/icons/Send'
import Image from '@material-ui/icons/Image'
import Remove from '@material-ui/icons/Remove'
import { useState, useEffect, useRef } from 'react'
import { API_URL } from "../../constant"
import axios from "axios"
import Cookies from 'js-cookie'
import Anonymous from "../../assets/anonymous.jpg";

const Chatroom = (props) => {

    const id = parseInt(Cookies.get("userId"));

    // handle chat body
    const [input, setInput] = useState("");
    const [chat, setChat] = useState([]);

    const [isLoading, setLoading] = useState(null);

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (input !== "" || selectedFile) {
            const newChat = {
                message: input !== "" ? input : null,
                // imgUrl: selectedFile ? preview : null,
                from: props.data.my_id,
                to: props.data.id,
                chatroom_id: props.data.chatroom_id
            }
            setChat(prev => [...prev, newChat])
            setInput('')
            setSelectedFile(undefined)


            try {
                const res = await axios.post(API_URL + "chat/" + props.data.chatroom_id + "/newMessage", newChat, {
                    withCredentials: true,
                })
                props.updateChatroom(props.data, newChat.message);
            }
            catch (e) {
                console.log(e.response);
            }

        }
    }

    // handle chat body ends

    // handle smooth scroll 
    const messageEndRef = useRef(null)

    const scrollToBottom = () => {
        messageEndRef.current?.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" })
    }

    useEffect(() => {
        scrollToBottom()
    }, [props.chatmsg])

    // handle smooth scroll ends

    // handle image upload
    const [selectedFile, setSelectedFile] = useState()
    const [preview, setPreview] = useState()

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)

        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const onSelectFile = e => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined)
            return
        }

        setSelectedFile(e.target.files[0])
    }



    return (
        <div className="chatroom">
            <div className="chatroom-header">
                <img src={API_URL + "image/" + props.data.imageUrl ?? Anonymous} alt={props.data.nickname} style={{ backgroundColor: props.data.profileColor }} />
                <div className="chat-header-info">
                    <h5>{props.data.nickname ? props.data.nickname : props.data.nickname}</h5>
                    <div className="active-status">
                        <p>{props.data.active ? "Active now" : "Not active"}</p>
                        {
                            props.data.active
                                ? <div className="active-circle"></div>
                                : null
                        }
                    </div>
                </div>
                <div className="chatroom-edit-btn">
                    <MoreHoriz></MoreHoriz>
                </div>
            </div>


            <div className="chatroom-body" style={{ height: selectedFile ? "70%" : "78%" }}>
                {props.chatmsg ? props.chatmsg.map((e) => {
                    return (
                        <>
                            {
                                e.message != null
                                    ? <div className="message" style={{ alignSelf: e.from === id ? "flex-end" : "flex-start", backgroundColor: e.from === id ? "#36AEC9" : "#2B3340" }}>
                                        <p>{e.message}</p>
                                    </div>
                                    : null
                            }

                        </>
                    )
                }) : null}
                <div ref={messageEndRef}></div>

            </div>

            <form className="chatform" onSubmit={(e) => handleSubmit(e)}>
                <div class="chat-input">
                    {
                        selectedFile &&
                        <div className="chat-img-wrap">
                            <Remove class="remove-btn" onClick={() => setSelectedFile(undefined)}></Remove>
                            <img src={preview} />
                        </div>
                    }
                    <input type="text" placeholder="Aa" value={input} onChange={(event) => setInput(event.target.value)} />
                </div>
                <div className="chat-input-buttons">
                    <input type="file" name="chatImage" id="chatImage" onChange={onSelectFile} style={{ display: "none" }} />
                    <label for="chatImage">
                        <Image className="chatroom-icon"></Image>
                    </label>
                    <Send className="chatroom-icon" onClick={(e) => handleSubmit(e)}></Send>
                </div>
            </form>
        </div>
    )
}

export default Chatroom;
