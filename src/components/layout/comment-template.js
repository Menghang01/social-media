import React, { useState, useEffect } from 'react';
import axios from "axios";
import { API_URL } from '../../constant';
import MoreHoriz from '@material-ui/icons/MoreHoriz'
import ReplyIcon from '@material-ui/icons/Reply';
import CommentInput from "./comment_input";

const CommentTemplate = ({ e, index, commentList, profile, handleInstantReply }) => {
    const [showReply, setShowReply] = useState(false)
    const [em, setEm] = useState(null)

    useEffect(() => {
        if (e !== undefined) {
            setEm(e);
        }

    }, [e]);


    return (
        <div className="comment-list" key={index}>
            <div className="comment-layout">
                <div className="comment-detail-image">
                    {
                        em ? <img src={API_URL + "image/" + em.profile.imageUrl} alt="" /> : null

                    }
                </div>
                <div className="comment-setting">
                    <div className="comment-detail-body" style={{ marginBottom: showReply ? "1rem" : "0" }}>
                        <div className="comment-wrapper">
                            <h6>{e.profile.nickname}</h6>
                            <p>{e.body}</p>
                        </div>
                        <div className="comment-methods">
                            <p>{e.difference_time}</p>
                            <ReplyIcon className="reply-icon" onClick={() => setShowReply(!showReply)}></ReplyIcon>
                        </div>
                    </div>
                    <div className="horiz" style={{ marginBottom: showReply ? "0" : "0" }}><MoreHoriz></MoreHoriz></div>
                </div>
            </div>

            <div className="comment-replies">
                <div className="comment-replies-sizebox">
                    <div className="comment-replies-body">
                        {
                            commentList[index].replies.slice(1).map((c) => {
                                return (
                                    // <div className="comment-replies">
                                    <div className="comment-layout" style={{ marginTop: "0.9rem" }}>
                                        <div className="comment-detail-image">
                                            <img src={API_URL + "image/" + c.profile.imageUrl} alt="" />
                                        </div>
                                        <div className="comment-setting">
                                            <div className="comment-detail-body">
                                                <div className="comment-wrapper">
                                                    <p style={{ fontWeight: "bold" }}>{c.profile.nickname}</p>
                                                    <p>{c.body}</p>
                                                </div>
                                                <div className="comment-methods">
                                                    <p>{c.difference_time}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
            <div className="comment-replies" style={{ marginTop: "1rem", padding: "0", display: showReply ? "flex" : "none" }}>
                <div className="comment-replies-sizebox"></div>
                <CommentInput postId={e} handleInstantReply={handleInstantReply}></CommentInput>
            </div>
        </div>
    )
}

export default CommentTemplate;