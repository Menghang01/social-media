import React, { useState, useEffect } from 'react';
import { API_URL } from "../../constant"
import { useSelector } from 'react-redux';
import "../../scss/comment_input.scss"
import axios from "axios";
import Cookies from 'js-cookie';
import Anonymous from '../../assets/anonymous.jpg'

const CommentInput = (props) => {
    const [comment, setComment] = useState("");
    const profile = useSelector(state => state.profiles)

    const handleChange = (e) => {
        setComment(e.target.value);
    }

    const onKeyPress = async (e) => {
        if (e.which === 13) {
            if (props.type === "parent") {
                props.handleSubmit(comment);
            }
            else {
                try {
                    const hi = props.handleInstantReply(comment, props.postId.parent_id)
                    const response = await axios.post(API_URL + "comment/" + props.postId.post_id + "/" + props.postId.parent_id, {
                        body: comment
                    }, {
                        withCredentials: true
                    });
                    console.log(response);
                }
                catch (e) {
                    console.log(e)
                    console.log(e.response)
                }
            }

            setComment("")
        }
    }


    return (

        props.postId ?
            <div className="comment-input">
                <div className="comment-profile">
                    <img src={profile.imageUrl.length !== 0 ? API_URL + "image/" + profile.imageUrl : Anonymous} alt="" />
                </div>
                <div className="comment-input-text">
                    <input value={comment} type="text" placeholder="Write your comments" onChange={(e) => handleChange(e)} onKeyPress={onKeyPress} />
                </div>
            </div>
            : null
    );
}

export default CommentInput;
