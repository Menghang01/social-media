import React, { useEffect } from 'react';
import "../../scss/commentlist.scss"
import CommentTemplate from './comment-template';

const Commentlist = (props) => {

    return (
        props.commentList && props.commentList.length !== 0 ? props.commentList.map((e, index) => {
            return (
                <CommentTemplate e={e} index={index} commentList={props.commentList} key={index} handleInstantReply={props.handleInstantReply} />
            )
        }) : null
    );

}

export default Commentlist;
