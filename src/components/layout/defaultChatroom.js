import Mailbox from '../../assets/graphics/mailbox.svg'

const DefaultChatroom = () => {
    return (
        <div className="default-chatroom">
            <img src={Mailbox} alt="mailbox" />
            <h3>Your Messages</h3>
            <p>Send private photos and messages to a friend or group</p>
            <div className="send-msg-btn">Send Message</div>
        </div>
    )
}

export default DefaultChatroom;