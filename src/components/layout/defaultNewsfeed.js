import React from 'react';
import Social from '../../assets/graphics/social.svg';

const DefaultNewsfeed = () => {

    const refreshPage = ()=>{
        window.location.reload();
    }

    return (
        <div className="default-newsfeed">
            <img src={Social} alt="social" />
            <h3>Nothing to show here</h3>
            <p>Refresh page or add some friends to see your newsfeed</p>
            <div className="refresh-btn" onClick={refreshPage}>Refresh</div>
        </div>
    )
}

export default DefaultNewsfeed
