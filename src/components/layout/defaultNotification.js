import Notification from '../../assets/graphics/noti.svg'

const DefaultNotification = () => {
    return (
        <div className="default-noti">
            <img src={Notification} alt="social" />
            <h3>No notifications yet</h3>
            <p>Interact with your friends to receive notifications</p>
        </div>
    )
}

export default DefaultNotification;