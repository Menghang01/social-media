import React, { useState, useEffect } from "react";
import "../../scss/editprofile.scss";
import ClearIcon from "@material-ui/icons/Clear";
import Film from "../../assets/icons/film.svg";
import Image from "../../assets/icons/image.svg"
import Anonymous from "../../assets/anonymous.jpg"
import AddPhotoAlternate from '@material-ui/icons/AddPhotoAlternate'
import Cookies from "js-cookie";
import { API_URL } from "../../constant"
import axios from "axios"

const EditProfile = ({handleClose, data}) => {

    const username = Cookies.get('username')
    const [input, setInput] = useState("")
    const [preview, setPreview] = useState()
    const [selectedFile, setSelectedFile] = useState()
    const [profile, setProfile] = useState()
    const [nickname, setNickname] = useState(data.nickname);
    const [desc, setDesc] = useState(data.description);

    const handleInputChange = (e) => {
        setNickname(e.target.value)
    }

    const handleTextareaChange = (e) => {
        setDesc(e.target.value)
    }
    
    const handleEditProfile = async () => {

        const newPost = new FormData()
        
        if (selectedFile) {
            newPost.append("nickname", nickname)
            newPost.append("description", desc)
            newPost.append("imageUrl", selectedFile)
        } else {
            newPost.append("nickname", nickname)
            newPost.append("description", desc)
        }

        await axios.post(API_URL + "profile/" + data.id, newPost, {withCredentials: true})
        .then((e) => console.log(""))
        .catch((e) => console.log(e.response))

        window.location.reload();
        handleClose()
    }

    // handle image upload

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)
        console.log(objectUrl);
        console.log(selectedFile);

        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const onSelectFile = e => {
        if (!e.target.files || e.target.files.length === 0) {
            console.log("undefined");
            setSelectedFile(undefined)
            return
        }

        console.log(e.target.files[0]);
        // I've kept this example simple by using the first image instead of multiple
        setSelectedFile(e.target.files[0])
    }

    useEffect(() => {
        setProfile(JSON.parse(sessionStorage.getItem("authUser")))
        console.log(profile)
    }, [])

    return (
        <div className="edit-profile">
            <div className="edit-info">
                <div className="edit-image">
                    <div className="edit-image-icon">
                        <input type="file" name="chatImage" id="chatImage" onChange={onSelectFile} style={{ display: "none" }} />
                        <label for="chatImage">
                            <AddPhotoAlternate className="icon"/>
                        </label>
                    </div>
                    {
                        data.imageUrl !== undefined
                        ? <img src={selectedFile ? preview : API_URL + `image/${data.imageUrl}`} alt="profile" className="img-border" />
                        : <img src={Anonymous}  className="img-border" ></img>
                    }
                </div>
                <form className="edit-form">
                    <label>Nickname</label>
                    <input type="text" value={nickname} onChange={(e) => handleInputChange(e)}/>
                    <label>Bio</label>
                    <textarea type="text" value={desc} onChange={(e) => handleTextareaChange(e)}/>
                </form>
            </div>
            <div className="btn-rows">
                <div className="save-edit" onClick={handleEditProfile}>Save</div>
                <div className="cancel-edit" onClick={handleClose}>Cancel</div>
            </div>
        </div>
    );
};
export default EditProfile;
