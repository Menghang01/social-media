import React, { useState, useEffect } from 'react';
import "../../scss/friend.scss"
import axios from 'axios';
import { API_URL } from "../../constant";
import Cookie from "js-cookie";
import FriendLoading from '../loading/friends_skeleton';
import { useHistory } from 'react-router-dom'

const Friend = (props) => {
    const authUserId = Cookie.get("userId");
    const history = useHistory()

    const [followData, setFollowData] = useState([])
    const [trendingData, setTrendingData] = useState([])
    const [loading, setLoading] = useState(true);
    const [trendingLoading, setTrendingLoading] = useState(true);

    const fetchFollowData = async () => {
        try {
            const res = await axios.get(API_URL + "follow/" + authUserId + "/data")
            setFollowData(res.data)
        } catch (err) {
            console.log(err)
        }
        setLoading(false)
    }

    const fetchTrendingData = async () => {
        try {
            const res = await axios.get(API_URL + "follow/trending")
            setTrendingData(res.data)
        } catch (err) {
            console.log(err)
        }
        setTrendingLoading(false)
    }

    useEffect(() => {
        fetchFollowData()
        fetchTrendingData()
    }, [])

    return (
        <div className="friends-component" onClick={() => props.handleSearch(false)}>
            <div className="divider"></div>
            <div className="follow-list">
                <h2>Who to follow</h2>
                <p>See the trending accounts right now</p>
                {
                    trendingLoading
                        ? <FriendLoading isTrending={true} />
                        : [
                            trendingData.length !== 0
                                ? trendingData.map((e, index) => {
                                    return (
                                        <div className="follow" key={index}>
                                            <img src={API_URL + `image/${e.imageUrl}`} alt="" />
                                            <div className="follow-info">
                                                <h4>{e.nickname}</h4>
                                                <p>{e.followers} followers</p>
                                            </div>
                                            <div className="follow-btn" onClick={() => history.push('/profile/' + e.id)}>
                                                <p>View profile</p>
                                            </div>
                                        </div>
                                    )
                                })
                                : <div className="no-friends">
                                    <h4>There are no trending users</h4>
                                </div>
                        ]
                }
            </div>
            <div className="divider"></div>
            <div className="friend-list">
                <h2>Friends</h2>
                <p>See who's currently online</p>
                {
                    loading
                        ? <FriendLoading />
                        : [
                            followData.data.length !== 0
                                ? followData.data.map((e, index) => {
                                    return (
                                        <div className="friend" key={index}>
                                            <div className="friend-info">
                                                <img src={API_URL + `image/${e.imageUrl}`} alt="" />
                                                <h4>{e.nickname}</h4>
                                            </div>
                                            <div className="friend-status">
                                                <h5>active</h5>
                                                <div className="status" style={{ backgroundColor: "#93DB4A" }}> </div>
                                            </div>
                                        </div>
                                    )
                                })
                                : <div className="no-friends">
                                    <h4>No Friends to show here</h4>
                                </div>
                        ]
                }
            </div>
        </div>
    );
}

export default Friend;
