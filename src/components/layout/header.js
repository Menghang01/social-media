import React, { useEffect, useState } from 'react';
import "../../scss/header.scss";
import Anonymous from "../../assets/anonymous.jpg";
import Logo from "../../assets/logo4.png"
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import SearchIcon from '@material-ui/icons/Search';
import { useHistory } from 'react-router-dom';
import { logout } from '../../util/auth';
import Cookies from "js-cookie";
import { API_URL } from '../../constant';
import axios from 'axios';
import { useSelector } from 'react-redux';


const Header = (props) => {

    const history = useHistory()
    const username = Cookies.get('username')
    const id = Cookies.get('userId')
    const profile = useSelector(state => state.profiles);
    const [searchResult, setSearchResult] = useState([]);
    const [word, setWord] = useState("");
    const [query, setQuery] = useState()
    const handleLogout = async () => {

        try {
            const response = await axios.get("http://localhost:8000/sanctum/csrf-cookie", {
                withCredentials: true
            });

            if (response.status) {
                await axios.post(API_URL + "logout", "", {
                    withCredentials: true
                })
            }
        }

        catch (error) {
            console.log(error.response);
        }

        const status = logout()



        history.push("/login");
    }

    const handleSearch = async () => {
        try {
            const response = await axios.get(API_URL + "search?query=" + query);
            console.log(response);
            setSearchResult(response.data)
        }
        catch (e) {
            console.log(e.response);
        }
    }

    useEffect(() => {
        setSearchResult([])
        setQuery(word)
        if (word.length === 0)
            return
        props.handleSearch(true)
        const timeout = setTimeout(() => {
            if (query && query.length !== 0)
                handleSearch();
        }, 1000);
        return () => clearTimeout(timeout);
    }, [word]);


    return (
        <div className="header">
            <div className="logo">
                <img src={Logo} alt="Logo" />
            </div>

            <div className="search-bar">
                <SearchIcon id="search"></SearchIcon>
                <input type="text" placeholder="Search KK" onChange={(e) => setWord(e.target.value)} />
                {
                    searchResult.length !== 0 && word.length !== 0 && props.showSearch ? <div className="result" >
                        {
                            searchResult && searchResult.length !== 0 ?
                                searchResult.map((e, index) => {
                                    return <div className="container" onClick={() => props.handleSearch(false)}>
                                        <div className="profile-url" onClick={() => history.push("/profile/" + e.id)}>
                                            <img src={API_URL + "image/" + e.imageUrl} alt="" />
                                            <div>{e.nickname}</div>
                                        </div>

                                    </div>

                                })

                                : null
                        }
                    </div> : null
                }

            </div>

            <div className="account-control">
                <div className="account-control-profile" onClick={() => history.push('/profile/' + id)}>
                    <img src={profile.imageUrl.length !== 0 ? API_URL + "image/" + profile.imageUrl : Anonymous} alt={"profile"} />
                    <p>{profile.nickname ?? "Aoi Tsukasa"}</p>
                </div>

                <div className="account-control-icon" onClick={() => handleLogout()}>
                    <ArrowDropDownIcon style={{ color: 'white' }}></ArrowDropDownIcon>
                </div>
            </div>
        </div>

    );
}

export default Header;