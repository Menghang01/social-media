import '../../scss/msg.scss';
import Chatroom from './chatroom';
import { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import { API_URL } from "../../constant"
import Anonymous from "../../assets/anonymous.jpg";
import DefaultChatroom from "./defaultChatroom"
import ChatroomLoading from "../loading/chatroom_skeleton";
import { useSelector, useDispatch } from "react-redux";
import { setCurrentChatId } from '../../store/profile';




const Msg = () => {
    const [chatRoomId, setChatRoomId] = useState([1, 2]);
    const currentChatId = useSelector(state => state.chat);
    const [chatMessage, setChatMessage] = useState([]);
    const dispatch = useDispatch();
    const [selectedIndex, setSelectedIndex] = useState(null)
    const [selectedChatroom, setSelectedChatroom] = useState(currentChatId);
    const activeChatroom = useRef(0)
    const [isLoading, setLoading] = useState(true);
    const [chat, setChat] = useState()
    const [newRoom, setNewRoom] = useState(useSelector(state => state.chat));
    const [updating, setUpdating] = useState(null);
    const [old, setOld] = useState(null);
    const [room, setRoom] = useState([])

    const [show, setShow] = useState(false);

    const [newMessage, setMessage] = useState({});


    const updateChatroom = (data, last) => {
        let temp = chatRoomId;
        for (let i = 0; i < temp.length; i++) {
            if (temp[i].chatroom_id === data.chatroom_id) {
                let deleted = temp.splice(i, 1);
                deleted[0].lastMessage = last;
                deleted[0].isRead = 0;
                deleted[0].seen = 0;
                deleted[0].last_from = parseInt(Cookies.get("userId"))
                temp.unshift(deleted[0]);
            }
        }
        setChatRoomId([...temp]);
    }


    const getRoomMessage = async () => {
        try {
            const response = await axios.get(API_URL + "chat/" + currentChatId + "/messages", {
                withCredentials: true
            })
            setChatMessage(response.data);
        }
        catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {

        console.log(newRoom);
        setLoading(false);
        if (Object.keys(newRoom).length !== 0) {
            console.log("object");
            let temp = chatRoomId;
            temp.unshift(newRoom);
            setChatRoomId([...temp])
            setShow(true);
            console.log(chatRoomId)
        }
    }, [newRoom]);


    useEffect(() => {
        const getRoom2 = async () => {
            try {
                setLoading(true)
                const response = await axios.get(API_URL + "chat/rooms", {
                    withCredentials: true
                })
                console.log(response.data);
                setLoading(false)
                setShow(true);
                setChatRoomId(response.data)
                // if (Object.keys(newRoom).length !== 0) {
                //     let temp = chatRoomId;
                //     temp.unshift(newRoom);
                //     setChatRoomId([...temp])
                //     console.log(chatRoomId)
                // }

            }
            catch (e) {
                console.log(e.response);
            }
        }
        getRoom2()

    }, [])

    useEffect(() => {
        window.Echo.private("Chat").
            listen('.message.new', e => {

                if (e.chatMessage.chatroom_id === activeChatroom.current) {
                    if (old === null) {
                        setChatMessage(oldMessage => [...oldMessage, e.chatMessage])
                        if (e.chatMessage.from !== Cookies.get("userId")) {
                            updateChatroom(e.chatMessage, e.chatMessage.message)
                        }

                    }
                    else {
                        if (old.chatroom_id !== e.chatMessage.chatroom_id) {
                            setChatMessage(oldMessage => [...oldMessage, e.chatMessage])
                        }

                    }
                }
                setMessage(e.chatMessage);
            })
    }, []);

    useEffect(() => {
        if (Object.keys(newMessage).length !== 0) {
            let temp = chatRoomId;
            for (let i = 0; i < temp.length; i++) {

                if (temp[i].chatroom_id === newMessage.chatroom_id) {
                    let deleted = temp.splice(i, 1);
                    deleted[0].lastMessage = newMessage.message;
                    deleted[0].isRead = 0;
                    deleted[0].seen = 0;
                    deleted[0].last_from = newMessage.from;
                    temp.unshift(deleted[0]);
                }
            }
            setChatRoomId([...temp]);
            console.log(chatRoomId)


        }

    }, [newMessage]);



    useEffect(() => {
        if (selectedChatroom === 0) {
            return;
        }
        else {
            const getRoomMessage = async () => {
                if (typeof (selectedChatroom) === "object") {
                    return
                }
                try {
                    const response = await axios.get(API_URL + "chat/" + selectedChatroom + "/messages", {
                        withCredentials: true
                    })
                    setChatMessage(response.data);
                }
                catch (e) {
                    console.log(e.response);
                }
            }

            getRoomMessage();
        }



    }, [])






    useEffect(() => {
        if (selectedChatroom > 0) {
            getRoomMessage();
        }
    }, [selectedChatroom])





    const handleClick = async (e, index) => {
        dispatch(setCurrentChatId(e.chatroom_id))
        setSelectedChatroom(e.chatroom_id)
        activeChatroom.current = e.chatroom_id;
        setSelectedIndex(index)
        setChat(chatRoomId[index]);

        dispatch(setCurrentChatId(e.chatroom_id))


        if (e.isRead === "0") {
            let chat = [...chatRoomId]
            chat[index].isRead = "1"

            setChatRoomId(chat);
            try {
                const response = await axios.post(`${API_URL}chat/${e.chatroom_id}`, null, {
                    withCredentials: true
                })
            }
            catch (e) {
                console.log(e.response);
            }
        }
        else {
            return
        }
    }

    const diffTime = (timestamp) => {
        let diff = (new Date().getTime() - Date.parse(timestamp)) / 1000;
        if (diff < 60)
            return "Just Now"
        else if (diff > 60 && diff < 3600) {
            return Math.floor(diff / 60) + "mn";
        }
        else if (diff > 60) {
            return Math.floor(diff / 3600) + "h";
        }
    }

    console.log(chatRoomId);


    return (
        <div className="chat-container">
            <div className="chat-list">
                <div className="create-chat">
                    <h5>Start a new conversation </h5>
                </div>

                {
                    show == true && chatRoomId.map((e, index) => {
                        return (
                            <div className="chat-item" id={index === selectedIndex ? "active" : ""} onClick={() => handleClick(e, index)}>

                                {
                                    isLoading === null || isLoading === false ? <div className="chat-side">
                                        <img src={API_URL + "image/" + e.imageUrl ?? Anonymous} alt={e.username} />
                                        <div className="chat-info">
                                            <h5>{e.nickname ? e.nickname : e.nickname}</h5>
                                            <p style={{ color: e.isRead === "1" ? "#868585" : "#fff" }}>{e.lastMessage}</p>

                                        </div>
                                        {
                                            e.isRead === "1" || e.last_from === parseInt(Cookies.get('userId'))
                                                ? null
                                                : <div className="seen-status"></div>
                                        }

                                    </div> : <ChatroomLoading></ChatroomLoading>

                                }

                                <h6 style={{ color: e.seen ? "#868585" : "#fff" }}>{e.isRead === "1" || e.last_from === Cookies.get('userId')}</h6>
                            </div>
                        )
                    })}


            </div>
            {
                selectedIndex === null
                    ? <DefaultChatroom></DefaultChatroom>
                    : <Chatroom data={chat} chatmsg={chatMessage} chatRoomId={chatRoomId} updateChatroom={updateChatroom} updating={updating}></Chatroom>
            }

        </div>
    )
}

export default Msg;