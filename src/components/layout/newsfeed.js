import axios from 'axios';
import Cookie from 'js-cookie';
import React from 'react';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { API_URL } from "../../constant"
import "../../scss/newsfeed.scss"
import PostTemplate from './post-template';
import { logout } from '../../util/auth';
import DefaultNewsfeed from './defaultNewsfeed';
import NewsfeedLoading from '../loading/newsfeed_skeleton';
import { useSelector } from 'react-redux'
import Cookies from 'js-cookie';


const Newsfeed = () => {
    const user_profile = useSelector(state => state.profiles);

    const history = useHistory();

    const id = Cookie.get("userId")

    const [feed, setFeed] = useState([])
    const [loading, setLoading] = useState(true)
    const [userProfile, setUserProfile] = useState();
    const [newFeed, setNewFeed] = useState(useSelector(state => state.newPosts));
    console.log(feed)
    const profile = async () => {
        try {
            const res = await axios.get(API_URL + "profile/" + id, { withCredentials: true })
            setUserProfile(res.data);
        }
        catch (e) {
            logout()
            history.push('/login')
        }
    }
    useEffect(() => {
        window.Echo.private("App.Models.User." + Cookies.get('userId'))
            .notification((notification) => {
                if (notification.type === "App\\Notifications\\PostNotification") {
                    setNewFeed(notification.post);
                }

            }
            )

    }, [])

    useEffect(() => {
        if (Object.keys(newFeed).length !== 0) {
            let temp = feed;
            temp = [newFeed, ...temp];
            setFeed([...temp])
        }
    }, [newFeed]);

    const fetchPosts = async () => {
        await axios.get(API_URL + "post", { withCredentials: true })
            .then((e) => {
                setFeed(e.data)
            })
            .catch((e) => console.log(e.response))

        setLoading(false)
    }

    useEffect(() => {
        fetchPosts()
        profile()
    }, [])
    console.log(feed)
    return (
        <div className="newsfeed">
            <div className="feeds">
                {
                    loading && !userProfile
                        ? <NewsfeedLoading></NewsfeedLoading>
                        : [
                            feed.length === 0
                                ? <DefaultNewsfeed />
                                : feed.map((e, index) => {
                                    return (
                                        <PostTemplate data={e} key={index} profile={userProfile}></PostTemplate>
                                    )
                                })
                        ]
                }
            </div>
        </div>
    );
}

export default Newsfeed;