import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ClearIcon from "@material-ui/icons/Clear";
import Check from "@material-ui/icons/Check";
import Microphone from "../../assets/noti-icons/microphone.svg";
import Party from "../../assets/noti-icons/party.svg";
import Chat from "../../assets/noti-icons/chat-bubble.svg"
import Like from "../../assets/noti-icons/like.svg";
import Lightbulb from "../../assets/noti-icons/lightbulb.svg"
import { ContactsOutlined } from "@material-ui/icons";
import Anonymous from "../../assets/anonymous.jpg";
import { API_URL } from '../../constant';
import Echo from 'laravel-echo';
import Cookies from 'js-cookie';

const NotiTemplate = ({ data, id, time, type, read }) => {
    console.log()
    const [icon, setIcon] = useState();
    const [description, setDescription] = useState("")
    const [isRead, setIsRead] = useState(false)
    console.log(data);

    const initType = (type) => {
        switch (type.replace("App\\Notifications\\", "")) {
            case "CommentNotification":
                setIcon(Microphone)
                data.commentType === "comment"
                    ? setDescription("commented on your status: \"" + data.comment.body + "\"")
                    : setDescription("replied to your comment: " + "\"" + data.comment.body + "\"")
                break;
            case "ChatNotification":
                setIcon(Chat)
                setDescription(": \"" + data.message.message + "\"")
                break;
            case "LikeNotification":
                setIcon(Like)
                data.count !== 0
                    ? setDescription("and " + data.count + " other liked your post: " + "\"" + data.post.caption + "\"")
                    : setDescription("liked your post: " + "\"" + data.post.caption + "\"")
                break;
            case "PostNotification":
                setIcon(Party)
                setDescription("updated a new post: " + "\"" + data.post.caption + "\"");
                break;
            case "FollowNotification":

                setIcon(Lightbulb)
                    ? setDescription("starts following you")
                    : setDescription("unfollowed you")
                break;
           
            default:
                return;
        }
    }

    const onClearNoti = async () => {
        try {
            const res = await axios.post(API_URL + "notifications/" + id, "", { withCredentials: true })
            setIsRead(true)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        setIsRead(read !== null)
        initType(type)
    }, [])

    return (
        <div className="noti-row" style={{ color: isRead ? "#868585" : "white" }}>
            <div className="noti-content">
                <div className="noti-img">
                    <img src={icon} alt="" className="img-icons" />
                    <img src={data ? API_URL + "image/" + data.profile.imageUrl : Anonymous} alt="" className="img-border" />
                </div>
                <div className="noti-status">
                    <p className="noti-name">{data && data.profile.nickname}</p>
                    <p className="noti-info">{description}</p>
                    <div className="indicator" style={{ display: isRead ? "none" : "" }}></div>
                </div>
                <p className="time-status">{time}</p>
                <div className="noti-options" title="Mark as read">
                    <Check className="border" onClick={() => onClearNoti()}></Check>
                </div>
            </div>
        </div>
    )
}

export default NotiTemplate;