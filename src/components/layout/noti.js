import React, { useState, useEffect } from "react";
import axios from 'axios';
import "../../scss/notification.scss";
import DefaultNotification from './defaultNotification'
import Echo from 'laravel-echo';
import Cookies from 'js-cookie';
import { API_URL } from "../../constant"
import NotiTemplate from "./noti-template";
import NotiLoading from "../loading/noti_skeleton"
import { CodeSharp } from "@material-ui/icons";
import Anonymous from "../../assets/anonymous.jpg";
import ClearIcon from "@material-ui/icons/Clear";
import Microphone from "../../assets/noti-icons/microphone.svg";
import Like from "../../assets/noti-icons/like.svg";
import Party from "../../assets/noti-icons/party.svg";
import Chat from "../../assets/noti-icons/chat-bubble.svg"
import Lightbulb from "../../assets/noti-icons/lightbulb.svg"
import { setPost } from '../../store/profile';
import { useDispatch } from 'react-redux';
import Check from "@material-ui/icons/Check";


window.Pusher = require('pusher-js');
try {
    window.Echo = new Echo({
        authEndpoint: 'http://localhost:8000/api/broadcast',
        broadcaster: 'pusher',
        key: '5f8c93de3772e1596046',
        channel_name: "Chat",
        cluster: 'ap1',
        forceTLS: true,
        encrypted: true,
        auth: {
            headers: {
                'X-CSRF-TOKEN': Cookies.get('XSRF-TOKEN')
            }
        },

    });
}

catch (e) {
    console.log(e.response);
}

const Noti = () => {
    const [noti, setNoti] = useState([])
    const [newNoti, setNewNoti] = useState([])
    const [count, setCount] = useState(0)
    const [update, setUpdate] = useState([])
    const [from, setFrom] = useState("")
    const [showAlert, setShowAlert] = useState(false)
    const [loading, setLoading] = useState(true)
    const dispatch = useDispatch();


    const fetchNoti = async () => {
        try {
            const res = await axios.get(API_URL + "notifications", { withCredentials: true })
            setNoti(res.data);
        } catch (err) {
            console.log(err);
        }

        setLoading(false)
    }

    useEffect(() => {
        window.Echo.private("App.Models.User." + Cookies.get('userId'))
            .notification((notification) => {
                setType(notification.type, notification)
                setNewNoti(prev => [...prev, notification])
            }
            )
    }, [])
    useEffect(() => {
        fetchNoti()
    }, [])

    const setType = (type, notification) => {
        switch (type.replace("App\\Notifications\\", "")) {
            case "CommentNotification":
                notification.icon = Microphone
                notification.content = notification.commentType === "comment"
                    ? "commented on your status: \"" + notification.comment.body + "\""
                    : "replied to your comment: " + "\"" + notification.comment.body + "\""
                break;
            case "ChatNotification":
                notification.icon = Chat
                notification.content = ": \"" + notification.message.message + "\""
                break;
            case "LikeNotification":
                notification.icon = Like
                notification.content = notification.count !== 0
                    ? "and " + notification.count + " other liked your post: " + "\"" + notification.post.caption + "\""
                    : "liked your post: " + "\"" + notification.post.caption + "\""
                break;
            case "PostNotification":

                notification.icon = Party

                notification.content = "updated a new post: " + "\"" + notification.post.caption + "\""

                break;
            case "FollowNotification":
                notification.icon = Lightbulb
                notification.content = notification.followType === "follow"
                    ? "starts following you"
                    : "unfollowed you"
                break;
            default:
                return;
        }
    }

    return (
        <div className="noti-main-div">
            <div className="noti-list">
                {
                    newNoti
                        ? [...newNoti].reverse().map((e, index) => {
                            return (
                                <div className="noti-row" style={{ color: "white" }}>
                                    <div className="noti-content">
                                        <div className="noti-img">
                                            <img src={e.icon} className="img-icons" />
                                            <img src={API_URL + "image/" + e.profile.imageUrl} className="img-border" />
                                        </div>
                                        <div className="noti-status">
                                            <p className="noti-name">{e.profile.nickname}</p>
                                            <p className="noti-info">{e.content}</p>
                                        </div>
                                        <div className="indicator"></div>
                                        <p className="time-status">Just Now</p>
                                    </div>
                                </div>
                            )
                        })
                        : null
                }
                {
                    loading
                        ? <NotiLoading></NotiLoading>
                        : [
                            noti.length !== 0 || newNoti.length !== 0
                                ? noti.map((e, index) => {
                                    return (
                                        <NotiTemplate data={e.data} id={e.id} time={e.difference_time} type={e.type} read={e.read_at} key={index}></NotiTemplate>
                                    )
                                })
                                : <DefaultNotification key={1}></DefaultNotification>
                        ]
                }
            </div>
        </div>
    );
};

export default Noti;
