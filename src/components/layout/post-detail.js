import '../../scss/post-detail.scss'
import { API_URL } from "../../constant"
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import Commentlist from "./commentlist"
import CommentInput from "./comment_input"
import Anonymous from '../../assets/anonymous.jpg'
import MoreHoriz from '@material-ui/icons/MoreHoriz'
import ClearIcon from "@material-ui/icons/Clear";
import ModeComment from '@material-ui/icons/ModeComment'
import Pepe from '../../assets/profiles/pepe.png'

const PostDetail = ({ showDetail = true, data, profile = [] }) => {
    const [pepe, setPepe] = useState(false)
    const [likeCount, setLikeCount] = useState(data.likes)
    const [commentList, setCommentList] = useState(data.comments)
    const [postProfile, setPostProfile] = useState([])

    const dispatch = useDispatch()

    const onCloseClick = () => {
        dispatch({
            type: 'SET_SHOW'
        })
    }

    useEffect(() => {
        setPepe(data.isLiked)
    })

    return (
        <div style={{ display: showDetail ? "" : "none" }}>
            <div className="pDetail-background"></div>
            <div className="pDetail-content">
                <div className="pDetail-left">
                    {
                        data.imageUrl
                            ? [
                                data.imageUrl.substr(data.imageUrl.length - 3) === "mp4"
                                    ? <video src={API_URL + "image/posts/" + data.imageUrl} alt="post" width="100%" controls loop></video>
                                    : <img src={API_URL + "image/posts/" + data.imageUrl} alt="post" />
                            ]
                            : null
                    }
                </div>
                <div className="pDetail-right">
                    <div className="pDetail-header">
                        <div className="user-header">
                            <div className="profile-info">
                                <img src={profile.imageUrl ? API_URL + "image/" + profile.imageUrl : Anonymous} alt="Profile" />
                                <div>
                                    <h6>{profile.nickname ? profile.nickname : ""}</h6>
                                    <p>{data.difference_time}</p>
                                </div>
                            </div>
                            <div className="action-btns">
                                <div className="circle">
                                    <MoreHoriz></MoreHoriz>
                                </div>
                                <div className="circle" onClick={() => onCloseClick()}>
                                    <ClearIcon></ClearIcon>
                                </div>
                            </div>
                        </div>
                        <div className="content">
                            <p>{data.caption}</p>
                        </div>
                        <div className="divider"></div>
                    </div>
                    <div className="pDetail-comments">
                        <Commentlist commentList={data.comments} post_id={data.id}></Commentlist>
                    </div>
                    <div className="pDetail-input">
                        <div className="divider"></div>
                        <div className="react-bar">
                            <div className="pepe-react">
                                <img src={Pepe} alt="pepe" onClick={() => setPepe(!pepe)} style={{ filter: pepe ? "grayscale(0)" : "grayscale(1)" }} />
                                <p>{data.length != 0 ? data.likes.length : ""}</p>
                            </div>
                            <div className="comment">
                                <ModeComment></ModeComment>
                                <p>{data.length != 0 ? data.comments.length : ""}</p>
                            </div>

                        </div>
                        <div className="divider"></div>
                        <CommentInput postId={data.id}></CommentInput>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default PostDetail;