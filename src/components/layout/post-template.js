import { useState, useEffect } from 'react';
import '../../scss/post-template.scss'
import ProfileComponent from '../profile/profileComponent'
import MoreHoriz from '@material-ui/icons/MoreHoriz'
import Favorite from '@material-ui/icons/Favorite'
import ModeComment from '@material-ui/icons/ModeComment'
import Pepe from '../../assets/profiles/pepe.png'
import Anonymous from "../../assets/anonymous.jpg";
import LoadingPic from "../../assets/loading.png";
import { API_URL } from "../../constant"
import Commentlist from "./commentlist"
import CommentInput from "./comment_input"
import axios from "axios";
import Cookies from 'js-cookie';
import { useSelector, useDispatch } from 'react-redux';

const PostTemplate = ({ data, isProfile, profile }) => {
    const [pepe, setPepe] = useState(false)
    const [likeCount, setLikeCount] = useState(data.likes)
    const [commentList, setCommentList] = useState(data.comments)
    const authProfile = useSelector(state => state.profiles);
    const dispatch = useDispatch()

    const onPostClick = (data) => {
        dispatch({
            type: 'SET_INDEX',
            data: data,
            profile: data.profile,
        })
    }

    const handleSubmit = async (comment) => {
        try {
            setCommentList(prev => [...prev, {
                body: comment,
                difference_time: "just now",
                profile: authProfile,
                replies: []
            }])
            const response = await axios.post(API_URL + "comment/" + data.id, { body: comment, user_id: Cookies.get('userId') }, { withCredentials: true })

        }
        catch (e) {
            console.log(e.response);
        }
    }

    const handleInstantReply = (comment, parentId) => {
        let temp = commentList

        for (let i = 0; i < temp.length; i++) {

            if (temp[i].parent_id === parentId) {
                temp[i].replies = [...temp[i].replies, {
                    profile: authProfile,
                    body: comment
                }]
            }
        }

        setCommentList([...temp])



    }

    const handleLike = async () => {
        try {
            if (pepe === false) {
                setLikeCount(prev => [...prev, 3])
            }
            else {
                let temp = likeCount;
                temp.splice(0, 1)
                setLikeCount([...temp])
            }
            const res = await axios.post(API_URL + "like/" + data.id, {}, { withCredentials: true });
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        setPepe(data.isLiked)
    }, [])

    return (
        <div className="post" style={{ width: isProfile ? "95%" : "75%", left: isProfile ? "0" : "none" }}>
            <div className="post-header">
                <div className="post-profile">
                    <ProfileComponent image={data.profile.imageUrl ? API_URL + "image/" + data.profile.imageUrl : Anonymous} username={data.profile.nickname ?? "..."} id={data.profile.id}></ProfileComponent>
                    <p>{data.difference_time}</p>
                </div>
                <div className="edit-post">
                    <MoreHoriz></MoreHoriz>
                </div>
            </div>

            <div className="post-content">
                <p className="caption" style={{ marginBottom: data.imageUrl ? "1.5rem" : "0" }}>{data.caption}</p>
                <div className="media" onClick={() => onPostClick(data)}>
                    {
                        data.imageUrl
                            ? [
                                data.imageUrl.substr(data.imageUrl.length - 3) === "mp4"
                                    ? <video src={API_URL + "image/posts/" + data.imageUrl} alt="post" width="100%" controls loop></video>
                                    : <img src={API_URL + "image/posts/" + data.imageUrl} alt="post" />
                            ]
                            : null
                    }
                </div>
                {
                    data.videoUrl
                        ? <iframe src={data.videoUrl} title="video" allowFullScreen></iframe>
                        : null
                }
            </div>

            <div className="divider"></div>

            <div className="post-reacts">
                <div className="pepe-react">
                    <img src={Pepe} alt="pepe" onClick={() => { setPepe(!pepe); handleLike() }} style={{ filter: pepe ? "grayscale(0)" : "grayscale(1)" }} />
                    <p>{likeCount && likeCount.length}</p>
                </div>
                <div className="comment">
                    <ModeComment></ModeComment>
                    <p>{commentList && commentList.length}</p>
                </div>
            </div>

            <div className="divider"></div>
            <Commentlist commentList={commentList} post_id={data.id} profile={profile} handleInstantReply={handleInstantReply}></Commentlist>
            <CommentInput userProfile={profile} postId={data} type={"parent"} handleSubmit={handleSubmit}></CommentInput>

        </div>
    )
}

export default PostTemplate;