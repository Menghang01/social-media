import React, { useState, useEffect } from "react";
import "../../scss/createpost.scss";
import ClearIcon from "@material-ui/icons/Clear";
import Film from "../../assets/icons/film.svg";
import Image from "../../assets/icons/image.svg"
import Anonymous from "../../assets/anonymous.jpg"
import Remove from '@material-ui/icons/Remove'
import Cookies from "js-cookie";
import { API_URL } from "../../constant"
import axios from "axios"
import { useSelector } from "react-redux";
const Post = ({ showBox, handleClose }) => {

    const username = Cookies.get('username')
    const [input, setInput] = useState("")
    const [preview, setPreview] = useState()
    const [selectedFile, setSelectedFile] = useState()
    const profile = useSelector(state => state.profiles);

    const handleCreateNewPost = async () => {

        const newPost = new FormData()
        newPost.append("caption", input)
        newPost.append("imageUrl", selectedFile)
        console.log(API_URL + "post/");
        await axios.post(API_URL + "createPost", newPost, { withCredentials: true })
            .catch((e) => console.log(e))

        setInput("")
        handleClose()
    }

    const handleOnChange = (e) => {
        setInput(e.target.value)
    }

    // handle image upload

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)


        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const onSelectFile = e => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined)
            return
        }

        // I've kept this example simple by using the first image instead of multiple
        setSelectedFile(e.target.files[0])
    }



    return (
        <div style={{ display: showBox ? "" : "none" }}>
            <div className="create-post"></div>
            <div className="post-box">
                <div className="post-header">
                    <p>Create Post</p>
                    <ClearIcon
                        className="border close-btn"
                        onClick={handleClose}
                    ></ClearIcon>
                </div>
                <div className="post-main">
                    <div className="post-user">
                        <img src={profile ? API_URL + "image/" + profile.imageUrl : "anonymous.jpg"} alt="" className="img-border" />
                        <p>{username}</p>
                    </div>
                    <div className="post-text">
                        <textarea
                            value={input}
                            name="desc"
                            className="text"
                            id=""
                            placeholder={"Tell us what you want to share, " + username}
                            onChange={(e) => handleOnChange(e)}
                        ></textarea>
                        {
                            selectedFile &&
                            <div className="post-img-wrap">
                                <Remove class="post-remove-btn" onClick={() => setSelectedFile(undefined)}></Remove>
                                <img src={preview} alt="preview" />
                            </div>
                        }
                    </div>
                    <div className="post-icons">
                        <input type="file" name="chatImage" id="chatImage" onChange={onSelectFile} style={{ display: "none" }} />
                        <label for="chatImage">
                            <img src={Image} alt="" />
                        </label>
                        <img src={Film} alt="" />
                    </div>
                </div>
                <div className="post-footer">
                    <h4 className="post-btn" onClick={() => handleCreateNewPost()}>Post</h4>
                </div>
            </div>
        </div>
    );
};
export default Post;
