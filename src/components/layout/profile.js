import React, { useState, useEffect } from "react";
import "../../scss/profile_screen.scss";
import SettingsIcon from "@material-ui/icons/Settings";
import axios from "axios";
import { API_URL } from "../../constant";
import { useHistory, useParams } from "react-router-dom";
import Cookies from "js-cookie";
import PostTemplate from "./post-template";
import Anonymous from "../../assets/anonymous.jpg";
import ProfileLoading from "../loading/profile_skeleton";
import NewsfeedLoading from "../loading/newsfeed_skeleton";
import FriendLoading from "../loading/friends_skeleton";
import EditProfile from "./editProfile";
import { setCurrentChatId, createNewRoom } from '../../store/profile';
import { useDispatch, useSelector } from "react-redux";

const Profile = () => {
	const { id } = useParams();
	const history = useHistory();
	const authUserId = Cookies.get("userId");
	const [selectedIndex, setIndex] = useState(0);
	const dispatch = useDispatch()
	const [followStatus, setFollowStatus] = useState(false);
	const [followData, setFollowData] = useState([]);
	const [profile, setProfile] = useState();
	const [collection, setCollection] = useState([
		{
			type: "Post",
			data: [],
		},
		{
			type: "Photo",
			data: [],
		},
		{
			type: "Video",
			data: [],
		},
	]);
	const [post, setPost] = useState([]);
	const [isAuthUser, setAuthUser] = useState(false);
	const [content, setContent] = useState();

	const [loading, setLoading] = useState(true);
	const [followLoading, setFollowLoading] = useState(true);
	const [test, setTest] = useState(useSelector(state => state.chat));


	const checkAuthUser = () => {
		if (authUserId === id) setAuthUser(true);
	};

	const fetchProfile = async () => {
		await axios.get(API_URL + "profile/" + id, { withCredentials: true })
			.then((res) => setProfile(res.data[0]))
			.catch((err) => console.log(err))
		setLoading(false)
	}

	const fetchProfilePost = async () => {
		await axios.get(API_URL + "profile/" + id + "/post", { withCredentials: true })
			.then((res) => setPost(res.data))
			.catch((err) => console.log(err.response))
	}

	const handleFollow = async () => {
		setFollowData(prevState => ({
			...prevState,
			followers: followStatus ? followData.followers - 1 : followData.followers + 1
		}))
		setFollowStatus(!followStatus)

		await axios.post(API_URL + "follow/" + id, "", { withCredentials: true })
			.catch((err) => console.log(err.response))
	}

	const fetchFollowStatus = async () => {
		await axios.get(API_URL + "follow/" + id, { withCredentials: true })
			.then((res) => setFollowStatus(res.data.status))
			.catch((err) => console.log(err))
	}

	const fetchFollowData = async () => {
		await axios.get(API_URL + "follow/" + id + "/data")
			.then((res) => setFollowData(res.data))
			.catch((err) => console.log(err))
		setFollowLoading(false)
	}

	const handleMessage = async () => {
		try {
			const response = await axios.post(API_URL + "chat/rooms", {
				'first': Cookies.get('userId'),
				'second': profile.user_id
			}, { withCredentials: true });
			dispatch(createNewRoom(response.data[0]))
			history.push("/message")

		}
		catch (e) {
			console.log(e.response)
		}

	}

	const handleShowEdit = () => {
		setShowEdit(!showEdit)
	}

	const handleIndex = (index) => {
		setIndex(index);
	}
	useEffect(() => {
		fetchProfile();
		fetchProfilePost();
		fetchFollowStatus();
		fetchFollowData();
		checkAuthUser();
	}, []);


	useEffect(() => {
		let temp = [];

		post.forEach(p => {
			if (p.imageUrl === null) {
				collection[0].data.push(p);
				temp.push(p);

			}
			else if (p.imageUrl.substr(p.imageUrl.length - 3) === "mp4") {
				collection[2].data.push(p);

			}
			else {
				collection[1].data.push(p);
			}
		}
		)
		setContent(temp)
	}, [post])


	useEffect(() => {
		if (collection.length !== 0) {
			setContent(collection[selectedIndex]['data'])
		}

	}, [selectedIndex]);




	const [love, setLove] = useState(false)
	const [pepe, setPepe] = useState(false)
	const [showEdit, setShowEdit] = useState(false);

	return (
		<>

			<div className="profile-main">
				<div className="profile-banner" style={{ height: showEdit ? "40%" : "25%" }}>
					{
						loading
							? <ProfileLoading />
							: [
								showEdit
									? <EditProfile handleClose={handleShowEdit} data={profile} />
									: <>
										<div className="profile-img">
											{
												profile
													? <img src={API_URL + `image/${profile.imageUrl}`} alt="" className="img-border" />
													: <img src={Anonymous} className="img-border" ></img>
											}
										</div>
										<div className="profile-status">
											<div className="profile-follow-banner">
												<h1>{profile ? profile.nickname : null}</h1>
												<div className="profile-follow-btn" style={{ display: isAuthUser ? "none" : "block" }} onClick={() => handleFollow()}>{followStatus ? "Following" : "Follow"}</div>
												<div className="profile-follow-btn" style={{ display: isAuthUser ? "none" : "block" }} onClick={() => handleMessage()}>Message</div>
											</div>
											<p>{profile ? profile.description : null}</p>
											<div className="profile-follow">
												<div className="following">
													<h4>{followData ? followData.following : 0} &ensp;</h4>
													<p>following</p>
												</div>
												<div className="followers">
													<h4>{followData ? followData.followers : 0} &ensp;</h4>
													<p>followers</p>
												</div>
											</div>
										</div>
										<div className="profile-settings" style={{ display: isAuthUser ? "flex" : "none" }} onClick={() => setShowEdit(!showEdit)}>
											<SettingsIcon className="setting-border"></SettingsIcon>
										</div>
									</>
							]
					}
				</div>
				<div className="profile-secondary">
					<div className="profile-box">
						<div className="profile-bar">

							{
								collection && collection.map((e, index) => {
									return (
										<p className={selectedIndex === index ? "active" : "inactive"} onClick={() => handleIndex(index)} >{e.type}</p>
									)
								})
							}

						</div>
						<div className="profile-post">
							{
								loading
									? <NewsfeedLoading isProfile={true} />
									: [
										content.length !== 0
											? content.map((e, index) => {
												return (
													<PostTemplate data={e} isProfile={true} key={index}></PostTemplate>
												)
											})
											: <div class="no-post">
												<h3>Nothing to show here {selectedIndex !== null && content.length !== 0}</h3>
												<p>Create some new posts</p>
											</div>
									]

							}

						</div>
					</div>
					<div className="profile-friendlist">
						<p>Friends ({followData ? followData.followers : 0})</p>
						{
							followLoading
								? <FriendLoading />
								: [
									followData.data.length !== 0
										? followData.data.map((e, index) => {
											return (
												<div className="friend" onClick={() => {
													history.push("/profile/" + e.id)
													window.location.reload()
												}} key={index}>
													<img src={API_URL + `image/${e.imageUrl}`} alt="" className="img-border" />
													<p>{e.nickname}</p>
												</div>
											)
										})
										: <div class="no-friends-profile">
											<h3>No friends to show</h3>
											<p>Add some friends</p>
										</div>
								]
						}
					</div>
				</div>
			</div>
		</>
	);
};

export default Profile;