import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import "../../scss/sidebar.scss"
import Home from '@material-ui/icons/Home';
import Notification from '@material-ui/icons/Notifications';
import Message from '@material-ui/icons/Message';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Cookies from 'js-cookie';
import Echo from 'laravel-echo';
import { API_URL } from "../../constant"
import axios from 'axios';
import { setPost } from '../../store/profile';
import { useDispatch } from 'react-redux';

const Sidebar = ({ handleCreatePost, handleSearch }) => {
    const history = useHistory()
    const id = Cookies.get("userId")
    const [notiCount, setNotiCount] = useState(0)
    const dispatch = useDispatch();


    const fetchNoti = async () => {
        try {
            const res = await axios.get(API_URL + "notifications/unread", { withCredentials: true })
            setNotiCount(res.data.length);
        } catch (err) {
            console.log(err);
        }
    }

    useEffect(() => {
        window.Echo.private("App.Models.User." + Cookies.get('userId'))
            .notification((notification) => {

                setNotiCount(prevCount => prevCount + 1)
            }
            )

        // fetchNoti()
    }, [])
    useEffect(() => {
        fetchNoti()

    })

    const menu = [
        {
            title: "Newsfeed",
            icon: <Home className="icon"></Home>,
            route: "/"
        },
        {
            title: "Notification",
            icon: <Notification className="icon"></Notification>,
            route: "/notification"
        },
        {
            title: "Messages",
            icon: <Message className="icon"></Message>,
            route: "/message"
        },
        {
            title: "Profile",
            icon: <AccountCircle className="icon"></AccountCircle>,
            route: "/profile/" + id
        }
    ]

    return (
        <div className="sidebar" onClick={() => handleSearch(false)}>
            <h2>Menu</h2>
            {menu.map((e, index) => {
                return (
                    <div className="sidebar-btn" key={index} id={window.location.pathname === e.route ? "active-btn" : ""} onClick={() => history.push(e.route)}>
                        {e.icon}
                        <h3>{e.title}</h3>
                        <div className="noti" style={{ display: e.title !== "Notification" || notiCount === 0 ? "none" : "" }}>{notiCount}</div>
                    </div>
                )
            })}
            <div className="create-post-btn" onClick={() => handleCreatePost()}>
                <h4>Create Post</h4>
            </div>
        </div>
    );
}

export default Sidebar;
