import React, { useState, useEffect } from 'react';
import Header from "./header"
import Sidebar from "./sidebar"
import Friend from './friend';
import Post from "./post"
import PostDetail from "./post-detail"
import "../../scss/template.scss"

import { useSelector, useDispatch } from 'react-redux';

const Template = (props) => {

    const [show, setShow] = useState(false)
    const [showSearch, setSearch] = useState(true);
    const handleCreatePost = () => {
        setShow(!show)
    }

    const handleSearch = (status) => {
        setSearch(status);
    }
    const posts = useSelector(state => state.posts);

    return (
        <div className="template">
            <Header showSearch={showSearch} handleSearch={handleSearch}></Header>
            <div className="template-body">
                <Sidebar className="" handleCreatePost={handleCreatePost} handleSearch={handleSearch}></Sidebar>
                {props.component}
                {
                    props.type !== "message" && props.type !== "profile"
                        ? <Friend handleSearch={handleSearch}></Friend>
                        : null
                }
            </div>
            <Post showBox={show} handleClose={handleCreatePost}></Post>
            <PostDetail showDetail={posts.show} data={posts.data} profile={posts.profile} />
        </div >
    );
}

export default Template;
