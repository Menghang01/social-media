import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

export default function ChatroomLoading() {
    return (
        <div style={{ display: 'flex', width: '100%', justifyContent: 'spaceBetween', flexDirection: 'row', alignItems: 'center' }} >
            <Skeleton style={{ color: "white", backgroundColor: 'grey', marginRight: '20px' }} variant="circle" width={40} height={40} />
            <Skeleton style={{ color: "white", backgroundColor: 'grey' }} variant="text" width={310} height={10} />
        </div>
    );
}
