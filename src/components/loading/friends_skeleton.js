import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

function FriendSkeleton() {
    return (
        <div style={{width: "100%", display: "flex",justifyContent: "space-between", alignItems: "center", backgroundColor: "#191e27", borderRadius: "15px", padding: "1rem", margin: "1rem 0 0", boxSizing: "border-box"}}>
            <div style={{display: "flex", alignItems: "center",justifyContent: "space-between",  width: "50%"}}>
                <Skeleton animation="wave" variant="circle" width={40} height={40} style={{backgroundColor: "#2c2f36"}}/>
                <Skeleton animation="wave" height={20} width="60%" style={{backgroundColor: "#2c2f36" }} />
            </div>
            <Skeleton animation="wave" height={20} width={60} style={{marginRight: "1rem", backgroundColor: "#2c2f36"}} />
        </div>
    );
}

export default function FriendLoading({isTrending = false}) {
    return (
        isTrending 
        ? <>
            <FriendSkeleton></FriendSkeleton>
            <FriendSkeleton></FriendSkeleton>
        </>
        : <>
            <FriendSkeleton></FriendSkeleton>
            <FriendSkeleton></FriendSkeleton>
            <FriendSkeleton></FriendSkeleton>
        </>
    )
}