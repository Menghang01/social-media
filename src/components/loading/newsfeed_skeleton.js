import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

function PostSkeleton({isProfile = false}) {
    return (
        <div style={{width: isProfile ? '90%' : '75%', backgroundColor: "#191e27", borderRadius: "15px", padding: "1.5rem", marginBottom: "1rem", boxSizing: "border-box"}}>
            <div style={{display: "flex", alignItems: "center", marginBottom: "1rem"}}>
                <Skeleton animation="wave" variant="circle" width={40} height={40} style={{backgroundColor: "#2c2f36"}}/>
                <Skeleton animation="wave" height={20} width="30%" style={{ marginLeft: "1rem", backgroundColor: "#2c2f36" }} />
            </div>
            <Skeleton animation="wave" height={20} width="60%" style={{ marginBottom: "1rem", backgroundColor: "#2c2f36" }} />
            <Skeleton animation="wave" variant="rect" height={200} style={{backgroundColor: "#2c2f36", borderRadius: "15px", marginBottom: "1rem"}}/>
            
            <div  style={{display: "flex",}}>
                <Skeleton animation="wave" height={20} width={60} style={{marginRight: "1rem", backgroundColor: "#2c2f36"}} />
                <Skeleton animation="wave" height={20} width={60} style={{marginRight: "1rem", backgroundColor: "#2c2f36"}}/>
                <Skeleton animation="wave" height={20} width={60} style={{marginRight: "1rem", backgroundColor: "#2c2f36"}}/>
            </div>
        </div>
    );
}

export default function NewsfeedLoading({isProfile}) {
    return (
        isProfile
        ? <PostSkeleton isProfile={isProfile}></PostSkeleton>
        : <>
            <PostSkeleton></PostSkeleton>
            <PostSkeleton></PostSkeleton>
        </>
    )
}