import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

function NotiSkeleton() {
    return (
        <div style={{width: "100%", height: "5.5rem", display: "flex",justifyContent: "space-between", alignItems: "center", backgroundColor: "#191e27", borderRadius: "15px", padding: "1rem", margin: "1rem 0 0", boxSizing: "border-box"}}>
            <div style={{display: "flex", alignItems: "center", width: "60%"}}>
                <Skeleton animation="wave" variant="circle" width={45} height={45} style={{backgroundColor: "#2c2f36", marginRight: "1rem"}}/>
                <Skeleton animation="wave" height={20} width="60%" style={{backgroundColor: "#2c2f36" }} />
            </div>
            <div style={{display: "flex", alignItems: "center", width: "20%"}}>
                <Skeleton animation="wave" height={20} width="60%" style={{backgroundColor: "#2c2f36",  marginRight: "1rem"}} />
                <Skeleton animation="wave" variant="circle" width={40} height={40} style={{backgroundColor: "#2c2f36"}}/>
            </div>
        </div>
    );
}

export default function NotiLoading() {
    return (
        <>
            <NotiSkeleton></NotiSkeleton>
            <NotiSkeleton></NotiSkeleton>
            <NotiSkeleton></NotiSkeleton>
            <NotiSkeleton></NotiSkeleton>
            <NotiSkeleton></NotiSkeleton>
            <NotiSkeleton></NotiSkeleton>
        </>
    )
}