import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

function ProfileSkeleton() {
    return (
        <div style={{width: '100%', display: "flex", alignItems: "center", justifyContent: "space-between",backgroundColor: "#191e27", borderRadius: "15px", padding: "1.5rem 3rem", boxSizing: "border-box"}}>
            <div style={{display: "flex", alignItems: "center", marginBottom: "1rem"}}>
                <Skeleton animation="wave" variant="circle" width={120} height={120} style={{backgroundColor: "#2c2f36", marginRight: "1rem"}}/>
                <div  style={{display: "flex", flexDirection: "column"}}>
                    <Skeleton animation="wave" height={25} width={150} style={{marginRight: "1rem", marginBottom: ".7rem", backgroundColor: "#2c2f36"}} />
                    <Skeleton animation="wave" height={25} width={300} style={{marginRight: "1rem", marginBottom: ".7em", backgroundColor: "#2c2f36"}}/>
                    <div style={{display: "flex"}}>
                        <Skeleton animation="wave" height={25} width={80} style={{marginRight: "1rem", backgroundColor: "#2c2f36"}}/>
                        <Skeleton animation="wave" height={25} width={80} style={{marginRight: "1rem", backgroundColor: "#2c2f36"}}/>
                    </div>
                </div>
            </div>
            <Skeleton animation="wave" variant="circle" width={60} height={60} style={{backgroundColor: "#2c2f36"}}/>
        </div>
    );
}

export default function ProfileLoading() {
    return (
        <>
            <ProfileSkeleton></ProfileSkeleton>
        </>
    )
}