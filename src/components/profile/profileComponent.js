import React from 'react';
import { useHistory } from 'react-router-dom';

const ProfileComponent = (props) => {
    const history = useHistory();

    const handleProfileClick = () => {
        history.push('/profile/' + props.id)
    }


    return (
        <div style={{ display: "flex", alignItems: "center" }} onClick={handleProfileClick}>
            <img src={props.image} alt="Profile" />
            <h6>{props.username ?? "Unnamed"}</h6>
        </div>
    );
}

export default ProfileComponent;
