import React from 'react';
import Template from "../components/layout/template"
import Newsfeed from "../components/layout/newsfeed"
const Home = () => {

    return (
        <div>
        <Template component={<Newsfeed/>} type="newsfeed"></Template>
        </div>
    );
}

export default Home;
