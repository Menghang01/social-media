import React from 'react';
import { useState } from 'react'
import axios from "axios"
import "../scss/signup.scss"
import { API_URL } from "../constant"
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/Visibility';
import { login } from '../util/auth';
import S1 from "../assets/Memoji-22.png"
import S2 from "../assets/Memoji-07.png"
import S3 from "../assets/Memoji-20.png"
import S4 from "../assets/Memoji-10.png"
import { useHistory } from 'react-router';
import { SolarSystemLoading } from 'react-loadingg';
import { useDispatch } from 'react-redux';
import { setProfile } from '../store/profile';



const Signup = () => {
    const history = useHistory()
    const [userInfo, setUserInfo] = useState()
    const dispatch = useDispatch()

    const [emailError, setEmailError] = useState({
        valid: "",
        message: "",
    })
    const [visibility, setVisibility] = useState(false);
    const [loading, setLoading] = useState(false);

    const toggleChange = () => {
        setVisibility(!visibility)
        let visible = document.getElementById("password")
        if (visible.type === "password")
            visible.type = "text";
        else
            visible.type = "password";


    }

    const handleChange = (event) => {
        if (event.target.name === "email") {
            let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(event.target.value)) {
                const updateInfo = { ...userInfo }
                let field = event.target.name
                updateInfo[field] = event.target.value
                setUserInfo(updateInfo)
                setEmailError((prevState) => ({
                    ...prevState,
                    valid: false,
                    message: "Please Enter a valid email address"
                }))
            }
            else {
                const updateInfo = { ...userInfo }
                let field = event.target.name
                updateInfo[field] = event.target.value
                setUserInfo(updateInfo)

                setEmailError(prevState => ({
                    ...prevState,
                    valid: true,
                    message: "Please enter valid email address."

                }));
            }
            if (event.target.value.length === 0) {
                setEmailError((prevState) => ({
                    ...prevState,
                    valid: true,
                    message: "Please Enter a valid email address"
                }))

            }
        }
        else {
            const updateInfo = { ...userInfo }
            let field = event.target.name
            updateInfo[field] = event.target.value
            setUserInfo(updateInfo)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        setLoading(true)
        try {
            // const response = await axios.get("http://localhost:8000/sanctum/csrf-cookie", {
            //     withCredentials: true
            // });
            const response = await axios.get("http://localhost:8000/sanctum/csrf-cookie", {
                withCredentials: true
            })


            if (response.status === 204) {
                const res = await axios.post(API_URL + "login", userInfo, {
                    withCredentials: true
                })
                dispatch(setProfile(res.data))
                const users = await axios.get(API_URL + "users", {
                    withCredentials: true,
                })

                const status = login(users.data)
                if (status) {
                    setLoading(false)
                    history.push('/')
                }
            }
        }

        catch (error) {
            console.log(error.response);
        }

    }
    return (

        <div className="container">
            <form className="login">
                {
                    loading
                        ? <div className="loading">
                            <p>Logging in</p>
                            <SolarSystemLoading size={'large'} className="spinner" speed={1} />
                        </div>
                        : <div className="login-left">
                            <div className="login-greeting">
                                <h3>Welcome </h3>
                                <p>Log in below to get started</p>
                            </div>
                            <div className="login-form">

                                <div>
                                    {emailError.valid === false ?
                                        <div style={{ color: 'red' }} >{emailError.message}</div>
                                        : null}
                                    <input type="email" name="email" placeholder="Email" onChange={(event) => handleChange(event)} />

                                </div>
                                <div>
                                    <input type="password" name="password" id="password" placeholder="Password" onChange={(event) => handleChange(event)} />
                                    {
                                        visibility ?
                                            <VisibilityIcon className='visibility' onClick={toggleChange}></VisibilityIcon>
                                            :
                                            <VisibilityOffIcon className='visibility' onClick={toggleChange}></VisibilityOffIcon>

                                    }

                                </div>


                                <button onClick={(event) => handleSubmit(event)} className="sign-up">Log In</button>
                                <div className="divider"></div>
                                <div className="no-have-acc">
                                    {/* Not a member ? <span onClick={() => history.push("/signup")}>Sign up now ! </span> */}
                                    <p>Don't have an account?</p>
                                    <div className="no-have-acc-btn" onClick={() => history.push("/signup")}>Sign Up</div>
                                </div>

                            </div>
                        </div>
                }
            </form>

            <div className="login-side">
                <div className="yellow">
                    <img src={S1} alt="" />
                </div>

                <div className="pink">
                    <img src={S4} alt="" />

                </div>
                <div className="green">
                    <img src={S3} alt="" />

                </div>
                <div className="purple">
                    <img src={S2} alt="" />

                </div>

                <div className="half light-blue"></div>
                <div className="half light-pink"></div>
                <div className="half light-green"></div>
            </div>
        </div>
    );
}

export default Signup;
