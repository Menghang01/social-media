import React from 'react';
import Template from "../components/layout/template"
import Msg from '../components/layout/msg';
import Echo from 'laravel-echo';
import Cookies from 'js-cookie';


const profile = () => {
    return (
        <div>
            <Template component={<Msg />} type="message"></Template>
        </div>
    );
}

export default profile;
