import React from 'react';
import Template from "../components/layout/template"
import Noti from "../components/layout/noti"
const Notification = () => {
    return (
        <div>
        <Template component={<Noti/>}  type="notification"></Template>
        </div>
    );
}

export default Notification;