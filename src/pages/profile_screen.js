import React from 'react';
import Template from "../components/layout/template"
import Profile from "../components/layout/profile"
const ProfileScreen = () => {
    return (
        <div>
            <Template component={<Profile/>}  type="profile"></Template>
        </div>
    );
}

export default ProfileScreen;
