import React from 'react';
import { useState } from 'react'
import axios from "axios"
import "../scss/signup.scss"
import { useHistory } from 'react-router-dom';
import { API_URL } from "../constant"
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import CheckIcon from '@material-ui/icons/Check';
import S1 from "../assets/Memoji-22.png"
import S2 from "../assets/Memoji-07.png"
import S3 from "../assets/Memoji-20.png"
import S4 from "../assets/Memoji-10.png"


const Signup = () => {
    const history = useHistory()
    const [userInfo, setUserInfo] = useState()
    const [confirm, setConfirm] = useState()
    const [invalid, setInvalid] = useState(true)
    const [visibility, setVisible] = useState(false);

    const [emailError, setEmailError] = useState({
        valid: "",
        message: "",
    })


    const handleConfirm = (event) => {
        const updateInfo = { ...userInfo }

        let field = event.target.name
        updateInfo[field] = event.target.value
        setUserInfo(updateInfo)

        if (event.target.value === userInfo.password) {
            setConfirm(true)
            setInvalid(false)
        }
    }

    const toggleChange = () => {

        let visible = document.getElementById("passwords")
        setVisible(!visibility)
        if (visible.type === "password")
            visible.type = "text";
        else
            visible.type = "password";

    }

    const handleChange = (event) => {

        if (event.target.name === "email") {
            let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(event.target.value)) {
                const updateInfo = { ...userInfo }
                let field = event.target.name
                updateInfo[field] = event.target.value
                setUserInfo(updateInfo)
                setEmailError((prevState) => ({
                    ...prevState,
                    valid: false,
                    message: "Please Enter a valid email address"
                }))
            }
            else {
                const updateInfo = { ...userInfo }
                let field = event.target.name
                updateInfo[field] = event.target.value
                setUserInfo(updateInfo)

                setEmailError(prevState => ({
                    ...prevState,
                    valid: true,
                    message: "Please enter valid email address."

                }));

            }
        }
        else {
            const updateInfo = { ...userInfo }
            let field = event.target.name
            updateInfo[field] = event.target.value
            setUserInfo(updateInfo)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        try {
            const response = await axios.get("http://localhost:8000/sanctum/csrf-cookie", {
                withCredentials: true
            })
            if (response) {
                const res = await axios.post(API_URL + "signup", userInfo, {
                    header: {
                        'Content-type': "application/json"
                    },
                    withCredentials: true
                })

                history.push("/login")
            }

        }
        catch (e) {
            console.log(e.response);
        }

    }
    return (
        <div className="container">
            <form className="login">
                <div className="login-left">
                    <div className="login-greeting">
                        <h3>Welcome</h3>
                        <p>Become a part in Cambodia's first social media Platform</p>
                    </div>
                    <div className="signup-form">
                        <div>
                            <input required type="text" name="name" placeholder="Username" onChange={(event) => handleChange(event)} />
                        </div>
                        <div>
                            {emailError.valid === false ?
                                <div style={{ color: 'red' }} >{emailError.message}</div>
                                : null}
                            <input required type="email" name="email" placeholder="Email" onChange={(event) => handleChange(event)} />

                        </div>
                        <div>
                            <input required type="password" name="password" id="passwords" placeholder="Password" onChange={(event) => handleChange(event)} />
                            {
                                visibility ?
                                    <VisibilityIcon className='visibility' onClick={toggleChange}></VisibilityIcon>
                                    :
                                    <VisibilityOffIcon className='visibility' onClick={toggleChange}></VisibilityOffIcon>


                            }
                        </div>
                        <div>
                            <input required type="password" name="password_confirmation" placeholder="Confirm Password" onChange={(event) => handleConfirm(event)} />
                            {
                                confirm ? <CheckIcon style={{ color: 'green' }} className='visibility'></CheckIcon>
                                    : null
                            }

                        </div>


                        <button disabled={invalid} onClick={(event) => handleSubmit(event)} className="sign-up">Sign Up</button>

                        <div className="have-acc">
                            Already have an account ? <span onClick={() => history.push("/login")}>Login Here ! </span>
                        </div>

                    </div>
                </div>
            </form>

            <div className="login-side">
                <div className="yellow">
                    <img src={S1} alt="" />
                </div>

                <div className="pink">
                    <img src={S4} alt="" />

                </div>
                <div className="green">
                    <img src={S3} alt="" />

                </div>
                <div className="purple">
                    <img src={S2} alt="" />

                </div>

                <div className="half light-blue"></div>
                <div className="half light-pink"></div>
                <div className="half light-green"></div>
            </div>
        </div>
    );
}

export default Signup;
