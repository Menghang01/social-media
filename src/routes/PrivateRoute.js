import React from 'react';
import { Route,Redirect } from 'react-router-dom'
import {isAuthenticated} from "../util/auth"

function PrivateRoute ({component: Component, path, authenticated, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => {return isAuthenticated()
          ? <Component {...props} />
          : <Redirect to={{pathname: '/login', state: {from: props.location}}}/> }}
      />
    )
  }
export default PrivateRoute;
