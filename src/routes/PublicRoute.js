import React from 'react';
import {  Route } from 'react-router-dom';

const PublicRoute = ({ render: Component, ...rest }) => {
    return (
        <Route {...rest} render={(props) => (
            <div>
                <Component {...props} />
            </div>
        )}
        />

    );
}

export default PublicRoute;
