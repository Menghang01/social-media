import { combineReducers } from 'redux';
const SET_PROFILE = 'ADD_BIRD';

export function setProfile(profile) {
    return {
        type: SET_PROFILE,
        profile,
    }
}

const profile = {
    id: "",
    nickname: "",
    imageUrl: "",
    user_id: "",
}
function profiles(state = profile, action) {
    switch (action.type) {
        case SET_PROFILE:
            return {
                ...state,
                id: action.profile.id,
                nickname: action.profile.nickname,
                imageUrl: action.profile.imageUrl,
                user_id: action.profile.user_id,
            }

        default:
            return state;
    }
}

const post = {
    data: [],
    show: false,
}

let newChatRoom = {

}

export function createNewRoom(room) {
    return {
        type: "CREATE_ROOM",
        room
    }
}

function posts(state = post, action) {
    switch (action.type) {
        case 'SET_INDEX':
            return {
                ...state,
                data: action.data,
                profile: action.profile,
                show: !state.show
            }
        case 'SET_SHOW':
            return {
                ...state,
                show: !state.show
            }
        default:
            return state;
    }
}


let currentChatId = 0;


export function setCurrentChatId(id) {
    return {
        type: "SET_CHATID",
        id
    }
}


function chat(state = newChatRoom, action) {
    switch (action.type) {
        case 'SET_CHATID':
            return action.id
        case 'CREATE_ROOM':
            return action.room
        default:
            return state;
    }
}


let newPost = {

}

export function setPost(post) {
    return {
        type: "SET_POST",
        post
    }
}



function newPosts(state = newPost, action) {
   
    switch (action.type) {
        case 'SET_POST':
            return {
                ...newPost,

            }
        default:
            return state;
    }
}


const profileApp = combineReducers({
    profiles,
    posts,
    newPosts,
    chat
});

export default profileApp;
