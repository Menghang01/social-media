import Cookie from "js-cookie";

const isAuthenticated = ()=>{
    return Cookie.get('isAuthenticated') ? true : false;
}

const login = (res) => {
    Cookie.set('userId', res.id)
    Cookie.set('username', res.name)
    Cookie.set('isAuthenticated', true)

    return true
}

const logout = () => {
    if (Cookie.get('isAuthenticated') !== undefined) {
        Cookie.remove('isAuthenticated');
        Cookie.remove('userId')
        Cookie.remove('username')

        return true
    }
}

export {
    isAuthenticated, login, logout
}

