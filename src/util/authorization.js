import axios from "axios"
import {Redirect} from "react-router-dom"
import { logout } from "./auth"

const handleAuthorization = (error) => {
    if(error.status == 401) {
        <Redirect path="/login"/>
    }
}

// const handleAuthorizaton = () => {
//     axios.interceptors.request.use(response => response, error => {
//         if (error.response.status === 401) {
//             logout();
//             <Redirect path="/login"/>

//             return Promise.reject()
//         }

//         return Promise.reject(error)
//     })
// }

export {
    handleAuthorization
}